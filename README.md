
# Airbnb Amsterdam

Ashley O'Mahony | [ashleyomahony.com](http://ashleyomahony.com) | February 2019  

***

This project visualizes Airbnb listings in Amsterdam published on December 6th, 2018.

All the files of this project are saved in a [GitHub repository](https://github.com/ashomah/Airbnb-Amsterdam).  
